import { FinalStructureState } from './model/final-structure.model';
import * as fromSteps from './state/app-steps.reducer';

export interface State {
  steps: FinalStructureState;
}

export const stepsReducer = fromSteps.reducer;
