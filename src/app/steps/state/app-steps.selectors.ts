import { createFeatureSelector, createSelector } from '@ngrx/store';
import { FinalStructureState } from '../model/final-structure.model';

export const selectPersonalInformationState = createFeatureSelector<FinalStructureState>('steps');

export const selectPersonalData = createSelector(
  selectPersonalInformationState,
  (state: FinalStructureState) => state.finalStructure.personalInformationState
);

export const isPersonalInformationValid = createSelector(
  selectPersonalInformationState,
  (state: FinalStructureState) => state.finalStructure.personalInformationState.isValid
);

export const selectContactData = createSelector(
  selectPersonalInformationState,
  (state: FinalStructureState) => state.finalStructure.contactState
);

export const isContactDataValid = createSelector(
  selectPersonalInformationState,
  (state: FinalStructureState) => state.finalStructure.contactState.isValid
);

export const selectFinalStructureData = createSelector(
  selectPersonalInformationState,
  (state: FinalStructureState) => state
);

export const selectFinalStructureIsSuccess = createSelector(
  selectPersonalInformationState,
  (state: FinalStructureState) => state.isSuccess
);
