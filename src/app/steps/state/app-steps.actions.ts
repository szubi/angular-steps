import { createAction, props } from '@ngrx/store';
import { ContactState } from '../model/contact.model';
import { FinalStructureState } from '../model/final-structure.model';
import { PersonalInformationState } from '../model/personal-information.model';

export const firstActionPatch = createAction(
  '[Personal] patch value',
  props<{ personalInformationState: PersonalInformationState }>()
);

export const secondActionPatch = createAction(
  '[Contact] patch value',
  props<{ contactState: ContactState }>()
);

export const thirdActionPost = createAction(
  '[Final Step] create structure',
  props<{ payload: FinalStructureState }>()
);

export const thirdActionPostSuccess = createAction(
  '[Final Step] create success',
  props<{ isSuccess: boolean }>()
);

export const clearFinalStructureState = createAction(
  '[CLEAR FINAL STRUCTURE] create success'
);
