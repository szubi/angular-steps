import { FinalStructureState } from './../model/final-structure.model';
import cloneDeep from 'lodash-es/cloneDeep';
import { createReducer, on, Action } from '@ngrx/store';
import { PersonalInformationState } from '../model/personal-information.model';
import {
  firstActionPatch,
  secondActionPatch,
  thirdActionPost,
  thirdActionPostSuccess,
  clearFinalStructureState
} from './app-steps.actions';
import { ContactState } from '../model/contact.model';

const initialState: FinalStructureState = {
  finalStructure: {
    personalInformationState: {
      personalInformation: {
        firstName: '',
        lastName: '',
        age: null
      },
      isValid: false
    },
    contactState: {
      contact: {
        email: '',
        phone: ''
      },
      isValid: false
    }
  },
  isSuccess: false
};

const finalStructureReducer = createReducer(
  initialState,
  on(
    firstActionPatch,
    (state: FinalStructureState, { personalInformationState }: { personalInformationState: PersonalInformationState }) => {
      const newState: FinalStructureState = cloneDeep(state);
      newState.finalStructure.personalInformationState.personalInformation = personalInformationState.personalInformation;
      newState.finalStructure.personalInformationState.isValid = personalInformationState.isValid;
      return newState;
    }
  ),
  on(
    secondActionPatch,
    (state: FinalStructureState, { contactState }: { contactState: ContactState }) => {
      const newState: FinalStructureState = cloneDeep(state);
      newState.finalStructure.contactState.contact = contactState.contact;
      newState.finalStructure.contactState.isValid = contactState.isValid;
      return newState;
    }
  ),
  on(
    thirdActionPost,
    (state: FinalStructureState) => {
      return cloneDeep(state);
    }
  ),
  on(
    thirdActionPostSuccess,
    (state: FinalStructureState, { isSuccess }: ReturnType<typeof thirdActionPostSuccess>) => {
      const newState: FinalStructureState = cloneDeep(state);
      newState.isSuccess = isSuccess;
      return newState;
    }
  ),
  on(
    clearFinalStructureState,
    () => {
      return initialState;
    }
  )
);

export function reducer(finalStructureState: FinalStructureState = initialState, action: Action): FinalStructureState {
  return finalStructureReducer(finalStructureState, action);
}
