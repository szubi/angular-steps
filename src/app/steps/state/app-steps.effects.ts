import { FinalStructureState } from './../model/final-structure.model';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';
import { ThirdService } from '../third/third.service';
import { thirdActionPost, thirdActionPostSuccess } from './app-steps.actions';

@Injectable()
export class ThirdEffects {
  constructor(private actions$: Actions, private thirdService: ThirdService) { }

  @Effect()
  saveStructure$ = this.actions$
    .pipe(
      ofType(thirdActionPost),
      map((action: ReturnType<typeof thirdActionPost>) => action.payload),
      switchMap((finalStructure: FinalStructureState) => this.thirdService.saveStructure(finalStructure)),
      map((isSuccess: boolean) => thirdActionPostSuccess({ isSuccess }))
    );
}
