import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-steps',
  templateUrl: './app-steps.component.html'
})
export class AppStepsComponent implements OnInit {

  items: MenuItem[];

  constructor() { }

  ngOnInit(): void {
    this.items = [
      {
        label: '',
        routerLink: 'first'
      },
      {
        label: '',
        routerLink: 'second'
      },
      {
        label: '',
        routerLink: 'third'
      }
    ];
  }
}
