export interface PersonalInformationState {
  personalInformation: {
    firstName: string;
    lastName: string;
    age: number;
  };
  isValid: boolean;
}
