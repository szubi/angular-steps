import { ContactState } from '../model/contact.model';
import { PersonalInformationState } from '../model/personal-information.model';

export interface FinalStructureState {
  finalStructure: {
    personalInformationState: PersonalInformationState;
    contactState: ContactState;
  };
  isSuccess: boolean;
}
