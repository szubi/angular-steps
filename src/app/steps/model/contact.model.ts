export interface ContactState {
  contact: {
    email: string;
    phone: string;
  };
  isValid: boolean;
}
