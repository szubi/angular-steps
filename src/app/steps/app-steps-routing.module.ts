import { AppStepsComponent } from './app-steps.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FirstModule } from './first/first.module';
import { SecondModule } from './second/second.module';
import { ThirdModule } from './third/third.module';

const routes: Routes = [
  {
    path: '',
    component: AppStepsComponent,
    children: [
      { path: '', redirectTo: 'first', pathMatch: 'full' },
      { path: 'first', loadChildren: () => FirstModule },
      { path: 'second', loadChildren: () => SecondModule },
      { path: 'third', loadChildren: () => ThirdModule }
    ]
  },
  {
    path: '**',
    component: AppStepsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppStepsRoutingModule { }
