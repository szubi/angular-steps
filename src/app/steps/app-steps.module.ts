import { stepsReducer } from './app-steps.reducer';
import { AppStepsRoutingModule } from './app-steps-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { StepsModule } from 'primeng/steps';
import { AppStepsComponent } from './app-steps.component';
import { ThirdEffects } from './state/app-steps.effects';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

@NgModule({
  declarations: [
    AppStepsComponent
  ],
  imports: [
    CommonModule,
    AppStepsRoutingModule,
    ToastModule,
    StepsModule,
    StoreModule.forFeature('steps', stepsReducer),
    EffectsModule.forFeature([ThirdEffects]),
    StoreDevtoolsModule.instrument({
      maxAge: 25
    })
  ],
  providers: [MessageService],
})
export class AppStepsModule { }
