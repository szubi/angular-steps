import { ThirdComponent } from './third.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ButtonModule } from 'primeng/button';
import { CardModule } from 'primeng/card';
import { ThirdService } from './third.service';

const routes = [
  { path: '', component: ThirdComponent }
];

@NgModule({
  declarations: [
    ThirdComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CardModule,
    ButtonModule,
  ],
  providers: [ThirdService]
})
export class ThirdModule { }
