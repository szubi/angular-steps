import { FinalStructureState } from './../model/final-structure.model';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import * as fromSteps from '../app-steps.reducer';
import {
  isContactDataValid,
  selectFinalStructureData,
  selectFinalStructureIsSuccess,
} from '../state/app-steps.selectors';
import { clearFinalStructureState, thirdActionPost } from '../state/app-steps.actions';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-third',
  templateUrl: './third.component.html'
})
export class ThirdComponent implements OnInit, OnDestroy {
  finalStructure: FinalStructureState;
  isSuccess = false;

  finalStructureData$: Subscription;
  isfinalStructureSuccess$: Subscription;
  isContactDataValid$: Subscription;

  constructor(
    private router: Router,
    private store: Store<fromSteps.State>
  ) { }

  ngOnInit(): void {
    this.onRedirectWhenBeforeStepIsInvalid();

    this.finalStructureData$ = this.store.select(selectFinalStructureData)
      .subscribe(finalStructure => {
        this.finalStructure = finalStructure;
      });
  }

  ngOnDestroy(): void {
    this.finalStructureData$.unsubscribe();
    this.isContactDataValid$.unsubscribe();

    if (!!this.isfinalStructureSuccess$) {
      this.isfinalStructureSuccess$.unsubscribe();
    }
  }

  complete(): void {
    this.store.dispatch(thirdActionPost({ payload: this.finalStructure }));

    this.isfinalStructureSuccess$ = this.store.select(selectFinalStructureIsSuccess)
      .pipe(first())
      .subscribe(value => {
        this.isSuccess = value;
      });

    this.store.dispatch(clearFinalStructureState());
  }

  prevPage(): void {
    this.router.navigate(['steps/second']);
  }

  cancel(): void {
    this.store.dispatch(clearFinalStructureState());

    this.router.navigate(['/']);
  }

  private onRedirectWhenBeforeStepIsInvalid(): void {
    this.isContactDataValid$ = this.store.select(isContactDataValid)
      .pipe(first())
      .subscribe(isValid => {
        if (!isValid) {
          this.prevPage();
        }
      });
  }
}
