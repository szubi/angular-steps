import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { RouterModule } from '@angular/router';
import { FirstComponent } from './first.component';

const routes = [
  { path: '', component: FirstComponent }
];

@NgModule({
  declarations: [
    FirstComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    CardModule,
    ButtonModule
  ]
})
export class FirstModule { }
