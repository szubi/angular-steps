import { PersonalInformationState } from '../model/personal-information.model';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as fromSteps from '../app-steps.reducer';
import { Store } from '@ngrx/store';
import { clearFinalStructureState, firstActionPatch } from '../state/app-steps.actions';
import { selectPersonalData } from '../state/app-steps.selectors';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html'
})
export class FirstComponent implements OnInit, OnDestroy {
  firstForm: FormGroup = this.formBuilder.group({
    firstName: [null, [Validators.required]],
    lastName: [null, [Validators.required]],
    age: [
      null,
      [Validators.required, Validators.min(1), Validators.max(120)]
    ]
  });

  personalData$: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private store: Store<fromSteps.State>
  ) { }

  ngOnInit(): void {
    this.personalData$ = this.store
      .select(selectPersonalData)
      .subscribe((personalInformationState: PersonalInformationState) => {
        this.firstForm.patchValue(personalInformationState.personalInformation, { emitEvent: false });
      });
  }

  ngOnDestroy(): void {
    this.personalData$.unsubscribe();
  }

  nextPage(): void {
    const isValid: boolean = this.firstForm.valid;

    if (!isValid) {
      return;
    }

    const personalInformationState: PersonalInformationState = this.preparePayload(isValid);
    this.store.dispatch(firstActionPatch({ personalInformationState }));

    this.router.navigate(['steps/second']);
  }

  cancel(): void {
    this.store.dispatch(clearFinalStructureState());

    this.router.navigate(['/']);
  }

  private preparePayload(valid: boolean): PersonalInformationState {
    return {
      personalInformation: {
        firstName: this.firstForm.get('firstName').value,
        lastName: this.firstForm.get('lastName').value,
        age: this.firstForm.get('age').value
      },
      isValid: valid
    } as PersonalInformationState;
  }
}
