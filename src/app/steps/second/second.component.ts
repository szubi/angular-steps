import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { isPersonalInformationValid, selectContactData } from '../state/app-steps.selectors';
import { clearFinalStructureState, secondActionPatch } from '../state/app-steps.actions';
import * as fromSteps from '../app-steps.reducer';
import { ContactState } from '../model/contact.model';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-second',
  templateUrl: './second.component.html'
})
export class SecondComponent implements OnInit, OnDestroy {
  secondForm: FormGroup = this.formBuilder.group({
    email: [null, [Validators.required]],
    phone: [null, [Validators.required]]
  });

  contactData$: Subscription;
  isPersonalValid$: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private store: Store<fromSteps.State>
  ) { }

  ngOnInit(): void {
    this.onRedirectWhenBeforeStepIsInvalid();

    this.contactData$ = this.store.select(selectContactData)
      .subscribe((contactState: ContactState) =>
        this.secondForm.patchValue(contactState.contact, { emitEvent: false })
      );
  }

  ngOnDestroy(): void {
    this.contactData$.unsubscribe();
    this.isPersonalValid$.unsubscribe();
  }

  nextPage(): void {
    const isValid: boolean = this.secondForm.valid;

    if (!isValid) {
      return;
    }

    const contactState: ContactState = this.preparePayload(isValid);
    this.store.dispatch(secondActionPatch({ contactState }));

    this.router.navigate(['steps/third']);
  }

  prevPage(): void {
    this.router.navigate(['steps/first']);
  }

  cancel(): void {
    this.store.dispatch(clearFinalStructureState());

    this.router.navigate(['/']);
  }

  private preparePayload(valid: boolean): ContactState {
    return {
      contact: {
        email: this.secondForm.get('email').value,
        phone: this.secondForm.get('phone').value
      },
      isValid: valid
    } as ContactState;
  }

  private onRedirectWhenBeforeStepIsInvalid(): void {
    this.isPersonalValid$ = this.store.select(isPersonalInformationValid)
      .pipe(first())
      .subscribe(isValid => {
        if (!isValid) {
          this.prevPage();
        }
      });
  }
}
