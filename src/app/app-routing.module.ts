import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppStepsModule } from './steps/app-steps.module';

const routes: Routes = [
  {
    path: 'steps',
    loadChildren: () => AppStepsModule
  },
  {
    path: '**',
    loadChildren: () => AppStepsModule
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
